from django.urls import path
from . import views

urlpatterns = [
    path('about/', views.about, name='about-page'),
    path('archive/', views.archive, name='archive-page'),
    path('contact/', views.contact, name='contact-page'),
    path('privacy/', views.privacy, name='privacy-page'),
    path('', views.PostList.as_view(), name='home'),
    path("<slug:slug>/", views.post_detail, name="post-page"),
]
